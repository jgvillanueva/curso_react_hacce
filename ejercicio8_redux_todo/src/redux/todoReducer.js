import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    todoList: [{id: 0, name: 'Añadir dispatchers'}],
    listaIndex: 0,
}

export const todoSlice = createSlice({
    name: 'todos',
    initialState,
    reducers: {
        add: (state, action) => {
            state.listaIndex ++;
            const todo = {
                id: state.listaIndex,
                name: action.payload
            };
            const lista = [...state.todoList];
            lista.push(todo);
            state.todoList = lista;
        },
        remove: (state, action) => {
            const newList = state.todoList.filter(todo => todo.id !== action.payload);
            state.todoList = [...newList];
        },
    }
})

export const {add, remove} = todoSlice.actions;
export default todoSlice.reducer;
