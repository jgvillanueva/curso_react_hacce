
import './App.css';
import {New} from "./components/New";
import {List} from "./components/List";

function App() {
  return (
    <div className="App">
      <New />
      <List />
    </div>
  );
}

export default App;
