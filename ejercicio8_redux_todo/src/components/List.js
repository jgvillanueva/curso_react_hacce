import {useDispatch, useSelector} from "react-redux";
import {remove} from "../redux/todoReducer";

export function List() {
    const todoList = useSelector((state) => {
        return state.todos.todoList;
    })
    const dispatch = useDispatch();


    const renderTodos = () => {
        return todoList.map(todo => {
            return <li key={todo.id}>
                {todo.name}
                <button onClick={ () => {dispatch(remove(todo.id))} }>
                    Eliminar
                </button>
            </li>
        });
    }

    return (
        <div>
            <h2>ToDos</h2>
            {renderTodos()}
        </div>
    )
}
