import {useRef} from "react";
import {useDispatch} from "react-redux";
import {add} from "../redux/todoReducer";

export function New() {
    const nombre = useRef();
    const dispatch = useDispatch();

    const handleClick = (event) => {
        event.preventDefault();
        dispatch(add(nombre.current.value));
    }
    return (
        <div>
            <h2>New ToDo</h2>
            <form>
                <input placeholder="nombre" ref={nombre}/>
                <button onClick={handleClick}>Add</button>
            </form>
        </div>
    )
}
