class DataService {
    async getAll() {
        const url = 'http://dev.contanimacion.com/api_tablon/api/mensajes';
        const response = await fetch(url, {
            method: 'GET',
        })
        return response;
    }

    async addMensaje(mensaje) {
        const url = 'http://dev.contanimacion.com/api_tablon/api/mensajes/add';
        const response = await fetch(url, {
            method: 'POST',
            body: JSON.stringify(mensaje),
            headers: {"Content-type": "application/json"}
        })
        return response;
    }
}
const dataService = new DataService();
export default dataService;
