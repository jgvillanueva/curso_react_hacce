import {useRef} from "react";
import {useDispatch, useSelector} from "react-redux";
import {addMensaje} from "../redux/MensajesReducer";

export default function New() {
    const  {error, loading, result} = useSelector((state) => {
        return {...state.mensajesReducer}
    })

    const asunto = useRef();
    const dispatch = useDispatch();

    const handleClick = (event) => {
        event.preventDefault();
        dispatch(addMensaje({
            asunto: asunto.current.value,
            mensaje: 'nada',
            user_id: 13,
        }));
    }
    return (
        <div>
            <h2>New ToDo</h2>
            {loading ? <h2>Cargando...</h2> : null}
            {error ? <h2>{error}</h2> : null}
            {result ? <h2>{result}</h2> : null}
            <form>
                <input placeholder="asunto" ref={asunto}/>
                <button onClick={handleClick}>Add</button>
            </form>
        </div>
    )
}
