import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {getMensajes} from "../redux/MensajesReducer";

export default function Lista () {
    const  {listaMensajes, error, loading} = useSelector((state) => {
        return {...state.mensajesReducer}
    })

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getMensajes());
    }, [dispatch]);

    const getItems = () => {
        return listaMensajes.map(mensaje =>
            <li key={mensaje.id}>{mensaje.asunto}</li>);
    }

    return (
        <div>
            {loading ? <h2>Cargando...</h2> : null}
            {error ? <h2>{error}</h2> : null}
            {getItems()}
        </div>
    )
}
