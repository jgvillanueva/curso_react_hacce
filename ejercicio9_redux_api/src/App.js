
import './App.css';
import Lista from "./components/Lista";
import New from "./components/New";

function App() {
  return (
    <div className="App">
      <h2>Mensajes</h2>

      <New />
      <Lista />
    </div>
  );
}

export default App;
