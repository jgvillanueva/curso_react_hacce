import {configureStore} from "@reduxjs/toolkit";
import mensajesReducer from "./MensajesReducer";

export const store = configureStore({
    reducer: {
        mensajesReducer: mensajesReducer,
    }
});
