import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import dataService from "../services/Data.service";

export const getMensajes = createAsyncThunk(
    'getMensajes',
    async () => {
        const response = await dataService.getAll();
        return response.json();
    }
);

export const addMensaje = createAsyncThunk(
    'addMensaje',
    async (mensaje) => {
        const response = await dataService.addMensaje(mensaje);
        return response.json();
    }
);

const initialState = {
    listaMensajes: [],
    error: '',
    loading: false,
    result: false,
}

export const mensajesSlice = createSlice({
    name: 'mensajesReducer',
    initialState,
    extraReducers: {
        [getMensajes.pending]: (state, action) => {
            state.loading = true;
            state.error = '';
        },
        [getMensajes.fulfilled]: (state, action) => {
            state.loading = false;
            state.listaMensajes = [...action.payload];
            state.error = '';
        },
        [getMensajes.rejected]: (state, action) => {
            state.loading = false;
            state.error = action.error.message;
        },
        [addMensaje.pending]: (state, action) => {
            state.loading = true;
            state.error = '';
        },
        [addMensaje.fulfilled]: (state, action) => {
            state.loading = false;
            state.error = '';
            if (action.payload.errors) {
                state.result = action.payload.errors.asunto;
            } else {
                state.result = action.payload[0].ok?
                    'Mensaje añadido' : 'Fallo al añadir mensaje';
            }
        },
        [addMensaje.rejected]: (state, action) => {
            state.loading = false;
            state.error = action.error.message;
        },
    }
})

export default mensajesSlice.reducer;
