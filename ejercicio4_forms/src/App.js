
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import {Toggle} from "./components/Toggle/Toggle";
import {Controlled} from "./components/Controlled/Controlled";
import {UnControlled} from "./components/UnControlled/UnControlled";
import {useState} from "react";
import {FormikForm} from "./components/FormikForm/FormikForm";

function App() {
    const [showControlled, setShowControlled] = useState(true);
    const [rotulo1, setRotulo1] = useState(true);

    const toggleForm = (controlled) => {
      setShowControlled(controlled);
    }
    const cambiaRotulo = (rotulo) => {
        setRotulo1(rotulo);
    }

  return (
    <div className="App">
      <Toggle
          label="Tipo de formulario"
          opcion1="Controlled"
          opcion2="UnControlled"
          default={showControlled}
          handleChange={toggleForm}
      />
        {
            showControlled ?
                /* <Controlled /> */
                <FormikForm />
                :
                <UnControlled />
        }


        <Toggle
            label="Cambio de rótulo"
            opcion1="Rótulo 1"
            opcion2="Rótulo 2"
            default={rotulo1}
            handleChange={cambiaRotulo}
        />
        {
            rotulo1 ?
                <h2>Rotulo 1</h2>
                :
                <h2>Rotulo 2</h2>
        }

    </div>
  );
}

export default App;
