import {createRef} from "react";
export function UnControlled() {
    const asuntoInput = createRef();
    const mensajeInput = createRef();

    const handleSubmit = (event) => {
        event.preventDefault();
        console.log(
            asuntoInput.current.value,
            mensajeInput.current.value,
        )
    }
    return (
        <div className='card m-3'>
            <div className="card-header">
                <h1>Uncontrolled form</h1>
            </div>
            <div className="card-body">
                <form className='formulario' onSubmit={handleSubmit} >
                    <div className='form-group' >
                        <label htmlFor="asunto">Asunto</label>
                        <input
                            type='text'
                            className='form-control'
                            name='asunto'
                            ref={asuntoInput}
                        />
                    </div>
                    <div className='form-group' >
                        <label htmlFor="mensaje">Mensaje</label>
                        <textarea
                            className='form-control'
                            name='mensaje'
                            ref={mensajeInput}
                        ></textarea>
                    </div>
                    <button className="btn btn-primary my-2" type="submit" >
                        Enviar
                    </button>
                </form>
            </div>
        </div>
    )
}
