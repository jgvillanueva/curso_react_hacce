import {useFormik} from "formik";

export function FormikForm() {
    const validar = (values) => {
        const errores = {};
        if (!values.asunto) {
            errores.asunto = 'Asunto es obligatorio';
        }
        if (!values.mensaje) {
            errores.mensaje = 'Mensaje es obligatorio';
        } else if (values.mensaje.length < 15) {
            errores.mensaje = 'El mensaje debe contener más de 15 caracteres';
        }
        return errores;
    };

    const formik = useFormik({
        onSubmit: (valores) => {
            console.log((valores));
        },
        initialValues: {
            asunto: '',
            mensaje: '',
        },
        validate: validar,
    });

    return (
        <div className='card'>
            <div className="card-header">
                <h1>Formik form</h1>
            </div>
            <div className="card-body">
                <form
                    className='formulario'
                    onSubmit={formik.handleSubmit}
                >
                    <div className='form-group' >
                        <label htmlFor="asunto">Asunto</label>
                        <input
                            type='text'
                            className='form-control'
                            name='asunto'
                            value={formik.values.asunto}
                            onChange={formik.handleChange}
                        />
                        {
                            formik.errors.asunto ?
                                <div className="alert alert-danger">{formik.errors.asunto}</div>
                                :
                                null
                        }
                    </div>
                    <div className='form-group' >
                        <label htmlFor="mensaje">Mensaje</label>
                        <textarea
                            className='form-control'
                            name='mensaje'
                            value={formik.values.mensaje}
                            onChange={formik.handleChange}
                        ></textarea>
                        {
                            formik.errors.mensaje ?
                                <div className="alert alert-danger">{formik.errors.mensaje}</div>
                                :
                                null
                        }
                    </div>
                    <button
                        className="btn btn-primary my-2"
                        type="submit"
                        disabled={!formik.isValid || !formik.dirty}
                    >
                        Enviar
                    </button>
                </form>
            </div>
        </div>
    )
}
