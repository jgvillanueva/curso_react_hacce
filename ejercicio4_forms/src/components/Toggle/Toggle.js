import {useEffect, useState} from "react";
import styles from './Toggle.module.css';

export function Toggle(props) {
    const [toggle, setToggle] = useState();

    const handleClick = () => {
        const newToggle = !toggle;
        setToggle(newToggle);
        props.handleChange(newToggle);
    }

    useEffect(() => {
        setToggle(props.default);
    }, [props.default]);

    if (toggle === null) {
        return <span></span>
    }

    const getButtonClass = () => {
        const colorClass = toggle?
            "btn-primary"
            :
            "btn-success";
        return `btn mx-2 ${colorClass}`;
    }

    return (
        <div className="my-2">
            <span className={styles.label}>{props.label}</span>
            <button
                className={getButtonClass()}
                onClick={handleClick}
            >
                {
                    toggle?
                        <span>{props.opcion1}</span>
                        :
                        <span>{props.opcion2}</span>
                }
            </button>

        </div>
    )
}
