import {useEffect, useState} from "react";
import ErrorComponent from "../ErrorComponent/ErrorComponent";

export function Controlled() {
    const [data, setData] = useState({
        asunto: '',
        mensaje: '',
        user_id: '12',
    });

    const [errores, setErrores] = useState({
        asunto: '',
        mensaje: '',
    });

    const [dirty, setDirty] = useState({
        asunto: false,
        mensaje: false,
    });

    const [formValid, setFormValid] = useState(false);

    const handleInputChange = (event) => {
        const newData = {...data};
        newData[event.target.name] = event.target.value;
        setData(newData);

        validarCampo(event.target.name, event.target.value);
    }

    const validarCampo = (nombre, valor) => {
        const newErrores = {...errores};
        let valido;
        let error = '';
        switch (nombre) {
            case 'asunto':
                valido = valor && valor.length > 5;
                error = valido ? '' : 'El asunto debe tener más de 5 caracteres';
                break;
            case 'mensaje':
                valido = valor && valor.length > 15;
                error = valido ? '' : 'El asunto debe tener más de 15 caracteres';
                break;
            default:
                break;
        }
        newErrores[nombre] = error;
        setErrores(newErrores);
        const newDirty = {...dirty};
        newDirty[nombre] = true;
        setDirty(newDirty);
        validaForm(newErrores, newDirty);
    }

    const validaForm = (newErrores, newDirty) => {
        let valido = true;
        Object.keys(newErrores).forEach((campo) => {
            if (!newDirty[campo] || (newErrores[campo] !== '' && newDirty[campo])) {
                valido = false;
            }
        })
        setFormValid(valido);
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        console.log(data);
    }

    return (
        <div className='card m-3'>
            <div className="card-header">
                <h1>Controlled form</h1>
            </div>
            <div className="card-body">
                <form className='formulario' onSubmit={handleSubmit}>
                    <div className='form-group' >
                        <label >Asunto</label>
                        <input
                            type='text'
                            className='form-control'
                            name='asunto'
                            value={data.asunto}
                            onChange={handleInputChange}
                        />
                        <ErrorComponent error={errores.asunto} />
                    </div>
                    <div className='form-group' >
                        <label >Mensaje</label>
                        <textarea
                            className='form-control'
                            name='mensaje'
                            value={data.mensaje}
                            onChange={handleInputChange}
                        ></textarea>
                        <ErrorComponent error={errores.mensaje} />
                    </div>
                    <div className='form-group' >
                        <label htmlFor='user_id'>User id</label>
                        <select
                            className='form-control'
                            name='user_id'
                            value={data.user_id}
                            onChange={handleInputChange}
                        >
                            <option value="11">Carmen</option>
                            <option value="12">Claudia</option>
                            <option value="13">Jorge</option>
                            <option value="14">David</option>
                        </select>
                    </div>
                    <button
                        className="btn btn-primary my-2"
                        type="submit"
                        disabled={!formValid}
                    >
                        Enviar
                    </button>
                </form>
            </div>
        </div>
    )
}
