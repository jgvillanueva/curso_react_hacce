import PropTypes from 'prop-types';
import {memo} from 'react';
function ErrorComponent (props) {
    if (props.error === '') {
        return null;
    }

    return (
        <div className="alert alert-danger my-3">
            {props.error}
        </div>
    )
}

const validadorError = (props, propName, componentName) => {
    const valor = props[propName];
    if(valor !== undefined) {
        return null;
    }
    return Error(`La proppiedad ${propName} es obligatoria en el componente ${componentName}`);
}

ErrorComponent.propTypes = {
    error: validadorError,
    //error: PropTypes.string.isRequired,
}
/*
ErrorComponent.defaultProps = {
    error: "Me falta el error",
}
*/

export default memo(ErrorComponent);
