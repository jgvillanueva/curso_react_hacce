
import {useState, useEffect} from "react";
import {useLocation, useParams, useNavigate} from "react-router-dom";

export function Detail() {
    const params = useParams();
    const location = useLocation();
    const navigate = useNavigate();

    const [mensaje, setMensaje] = useState({
        asunto: '',
        mensaje: '',
        created_at: '',
    });

    useEffect(() => {
        const getData = async () => {
            const id = params.id;
            const url = 'http://dev.contanimacion.com/api_tablon/api/mensajes/get/' + id;

            const data = await fetch(url, {method: 'GET'});
            const json = await data.json();
            setMensaje(json);
        }

        if(location.state) {
            const {data} = location.state;
            setMensaje(data);
        } else {
            getData();
        }
    }, [location.state, params.id]);

    const handleClick = () => {
        navigate('/lista');
        //navigate(-1);
    };

    return (
        <div className="detail">
            <h1>Detail</h1>
            <h2>{mensaje.asunto}</h2>
            <div>{mensaje.mensaje}</div>
            <div>{mensaje.created_at}</div>
            <button onClick={handleClick}>Volver</button>
        </div>
    )
}
