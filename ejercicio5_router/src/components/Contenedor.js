import {Link, Route, Routes} from "react-router-dom";
import {Lista} from "./Lista";
import {New} from "./New";
import {Home} from "./Home";
import {Detail} from "./Detail";
import {NoEncontrado} from "./NoEncontrado";

export function Contenedor() {
    return (
        <div className="contenedor">
            <header>
                <nav>
                    <ul>
                        <li><Link to="/" >Home</Link></li>
                        <li><Link to="/lista" >Lista</Link></li>
                        <li><Link to="/new" >Nuevo</Link></li>
                    </ul>
                </nav>
            </header>

            <div className="content">
                <Routes>
                    <Route path="/" index exact element={<Home />} />
                    <Route path="*" element={<NoEncontrado />} />
                    <Route path="/lista" exact element={<Lista />} />
                    <Route path="/lista/:id" element={<Detail />} />
                    <Route path="/new" element={<New />} />
                </Routes>
            </div>

        </div>
    )
}
