import {useCallback, useEffect, useMemo, useState, useTransition} from "react";
import {Link} from "react-router-dom";

export function Lista() {
    const [mensajes, setMensajes] = useState([]);
    const [filter, setFilter] = useState('');
    const [isPending, startTransition] = useTransition();

    const getData = async () => {
        const url = 'http://dev.contanimacion.com/api_tablon/api/mensajes';
        const data = await fetch(url, {method: 'GET'});
        const json = await data.json();
        return json;
    }
    const memoGetData = useCallback(getData, []);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const data = await memoGetData();
                setMensajes(data);
            } catch (e) {
                console.log("error recibiendo datos", e)
            }
        };

        fetchData();
    }, [memoGetData]);

    const renderMensajes = () => {
        return sortMensajes.map((mensaje) => {
            return <li key={mensaje.id}>
                <Link
                    to={'/lista/' + mensaje.id}
                    state={{
                        data: mensaje,
                    }}
                >
                    {mensaje.asunto}
                </Link>
            </li>
        })
    }

    const sortMensajes = useMemo(() => {
        if(!mensajes)
            return [];

        const mensajesOrdenados = mensajes.slice();
        mensajesOrdenados.sort((a, b) => b.created_at.localeCompare(a.created_at));
        return mensajesOrdenados;
    }, [mensajes])

    const handleChange = (event) => {
        startTransition(() => {
            setFilter(event.target.value);
        });
    }

    const getFiltered = () => {
        if(filter.length < 3) {
            return;
        }
        console.log("ejecutando getFiltered");
        if(isPending)
            return;

        console.log("filtrando mensajes");
        return sortMensajes.map((mensaje) => {
            if (mensaje.mensaje.indexOf(filter) > 0) {
                return <li key={mensaje.id}>
                    <Link to={'/lista/' + mensaje.id}>
                        {mensaje.asunto}
                    </Link>
                </li>
            }
        })
    }

    return (
        <div className="lista">
            <h1>Lista</h1>
            <input value={filter} onChange={handleChange}/>
            <h3>Mensajes filtrados</h3>
            <ul>{getFiltered()}</ul>
            <h3>Todos los mensajes</h3>
            {renderMensajes()}
        </div>
    )
}
