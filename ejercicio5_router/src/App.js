
import './App.css';
import {BrowserRouter} from "react-router-dom";
import {Contenedor} from "./components/Contenedor";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Contenedor />
      </BrowserRouter>
    </div>
  );
}

export default App;
