class DataService {
    async getAll() {
        return new Promise(async (resolve, reject) => {
            const url = 'http://dev.contanimacion.com/api_tablon/api/mensajes';
            await fetch(url, {
                method: 'GET',
            })
                .then(response => response.json())
                .then((response => {
                    resolve(response);
                }))
        })
    }
}
const dataService = new DataService();
export default dataService;
