import {createContext} from "react";

export const StringsContext = createContext({});
export const StringsProvider = StringsContext.Provider;

export const DataServiceContext = createContext({});
export const DataServiceProvider = DataServiceContext.Provider;
