import {useContext} from "react";
import {StringsContext} from "../Contexts.providers";

export function New () {
    const strings = useContext(StringsContext);
    return (
        <div>
            <h1>{strings.tituloNew}</h1>
        </div>
    )
}
