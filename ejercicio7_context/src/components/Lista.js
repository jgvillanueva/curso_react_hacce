import {useContext, useEffect, useState} from "react";
import {DataServiceContext, StringsContext} from "../Contexts.providers";

export function Lista () {
    const strings = useContext(StringsContext);

    const dataService = useContext(DataServiceContext);

    const [mensajes, setMensajes] = useState([]);

    useEffect(() => {
        const getData = async () => {
            const data = await dataService.getAll();
            setMensajes(data);
        }

        getData();
    }, [dataService]);

    const renderMensajes = () => {
        return mensajes.map((mensaje) => {
            return <li key={mensaje.id}>{mensaje.asunto}</li>
        })
    }

    return (
        <div>
            <h1>{strings.tituloLista}</h1>
            <ul>
                {renderMensajes()}
            </ul>
        </div>
    )
}
