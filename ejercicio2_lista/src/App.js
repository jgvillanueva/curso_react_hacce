import logo from './logo.svg';
import './App.css';
import {Lista} from "./components/Lista/Lista";
import Contador from "./components/Contador/Contador";

function App() {
    const usuarios = [
        {nombre: 'Jorge', edad: 72},
        {nombre: 'Paco', edad: 13},
        {nombre: 'Ana', edad: 33},
    ];

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Contador />
        <Lista data={usuarios} />
      </header>
    </div>
  );
}

export default App;
