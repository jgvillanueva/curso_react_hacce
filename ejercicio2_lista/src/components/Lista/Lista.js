import {Item} from "../Item/Item";

export function Lista(props) {
    console.log(props.data);

    const  getItems = () => {
        return props.data.map((usuario, index) => {
            return <Item  key={index} usuario={usuario} />
        });
    }

    return (
        <div className="lista">
            <h2>Mi primera lista</h2>
            <ul>
                { getItems() }
            </ul>
        </div>
    )
}

