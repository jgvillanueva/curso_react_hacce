import {useState} from "react";

export function Item (props) {
    const [expandido, setExpandido] = useState(false);

    const dibujaEdad = () => {
        if(expandido) {
            return <span> - {props.usuario.edad}</span>
        }
    }

    const handleClick = () => {
        const nuevoExpandido = !expandido;
        setExpandido(nuevoExpandido);
    }

    return (
        <li onClick={handleClick}>
            {props.usuario.nombre}
            {dibujaEdad()}
        </li>
    )
}
