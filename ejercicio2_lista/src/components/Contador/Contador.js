import React from "react";
class Contador extends React.Component {
    interval;

    constructor(props) {
        super(props);
        this.state = {
            numero: 0,
        };
    }

    componentDidMount() {
        this.interval = setInterval(() => {
            const nuevoNumero = this.state.numero + 1;
            this.setState({
                numero: nuevoNumero,
            })
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        return (
            <div className="contador">
                <h2>Contador</h2>
                <h3>{this.state.numero}</h3>
            </div>
        )
    }
}

export default Contador;
