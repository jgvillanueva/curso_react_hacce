import {createSlice} from "@reduxjs/toolkit";
export const APP_VIEWS = {
    lista: 'lista',
    new: 'new',
}
const initialState = {
    view: APP_VIEWS.lista,
}

export const appStatusSlice = createSlice({
    name: 'appStatusReducer',
    initialState,
    reducers: {
        changeView: (state, action) => {
            state.view = action.payload.view;
        },
    }
})

export const {changeView} = appStatusSlice.actions;
export default appStatusSlice.reducer;
