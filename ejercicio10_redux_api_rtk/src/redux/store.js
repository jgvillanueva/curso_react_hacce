import {configureStore} from "@reduxjs/toolkit";
import appStatusReducer from "./AppStatusReducer";
import {mensajesAPI} from "./mensajesAPI";
import {setupListeners} from "@reduxjs/toolkit/query";

export const store = configureStore({
    reducer: {
        appStatusReducer: appStatusReducer,
        [mensajesAPI.reducerPath]: mensajesAPI.reducer,
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({}).concat(mensajesAPI.middleware),
});

setupListeners(store.dispatch);
