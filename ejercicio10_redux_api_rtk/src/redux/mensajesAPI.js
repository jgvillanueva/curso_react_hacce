import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const mensajesAPI = createApi({
    reducerPath: 'mensajesAPI',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://dev.contanimacion.com/api_tablon/api/',
    }),
    tagTypes: ['mensajes'],
    endpoints: (builder) => ({
        getMensajes: builder.query({
            query: () => 'mensajes',
            providesTags: ['mensajes'],
        }),
        newMensaje: builder.mutation({
            query: (args) => ({
                url: 'mensajes/add',
                method: 'POST',
                body: args,
            }),
            invalidatesTags: ['mensajes'],
        })
    }),
});

export const {useGetMensajesQuery, useNewMensajeMutation} = mensajesAPI;
