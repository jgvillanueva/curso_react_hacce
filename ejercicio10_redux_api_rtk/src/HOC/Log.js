export default function Log(props) {
    const log = (texto) => {
        const hoy = new Date();
        console.log(`${hoy.getHours()}:${hoy.getMinutes()}:${hoy.getSeconds()}: ${texto}`);
    }


    return(
        <>
            {
                props.render({log,})
            }
        </>
    )
}
