import Log from "./Log";

export default function withLogs(WrappedComponent) {
    return function LogComponent({props}) {
        return (
            <Log render={({log}) => (
                <WrappedComponent log={log} />
            )}/>
        )
    }
}
