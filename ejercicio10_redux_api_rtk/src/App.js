
import './App.css';
import {useDispatch, useSelector} from "react-redux";
import {APP_VIEWS, changeView} from "./redux/AppStatusReducer";
import List from "./components/List";
import New from "./components/New";
import {ComponenteConLog} from "./HOC/ComponenteConLog";

function App() {
  const view = useSelector((state) => {
    return state.appStatusReducer.view
  });

  const dispatch = useDispatch();

  const renderView = () => {
    switch (view) {
      case APP_VIEWS.lista:
        return <ComponenteConLog />
      case APP_VIEWS.new:
        return <New />
      default:
        return <List />
    }
  }

  return (
    <div className="App">
      <div>
        <ul>
          <li onClick={() => {dispatch(changeView({view: APP_VIEWS.lista}))}}>Lista</li>
          <li onClick={() => {dispatch(changeView({view: APP_VIEWS.new}))}}>New</li>
        </ul>
      </div>
      {renderView()}
    </div>
  );
}

export default App;
