import {useRef} from "react";
import {useNewMensajeMutation} from "../redux/mensajesAPI";
import {useState} from "react";

export default function New() {
    const [newMensaje, requestStatus] = useNewMensajeMutation();
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);
    const [error, setError] = useState(false);

    const asunto = useRef();

    const handleClick = async (event) => {
        event.preventDefault();
        const response = await newMensaje(
            {
                asunto: asunto.current.value,
                mensaje: 'nada',
                user_id: 13,
            }
        );
        setIsLoading(requestStatus.isLoading);
        setIsError(requestStatus.isError);
        setError(requestStatus.error);
        console.log('requestStatus', requestStatus);
    }
    return (
        <div>
            <h2>New ToDo</h2>
            {isError ? <h3>Error: {error.error}</h3> : null }
            {isLoading ? <h3>Cargando...</h3> : null}
            <form>
                <input placeholder="asunto" ref={asunto}/>
                <button onClick={handleClick}>Add</button>
            </form>
        </div>
    )
}
