import {useGetMensajesQuery} from "../redux/mensajesAPI";

export default function List({log}) {
    const {data, isLoading, isError, error, status} = useGetMensajesQuery();

    log('*********************** en list');
    console.log('status:', status);
    console.log('error:', error);

    const renderItems = () => {
        if (!data) return null;
        return data.map(mensaje => <li key={mensaje.id}>{mensaje.asunto}</li>)
    }

    return (
        <div>
            <h2>Lista</h2>
            {isError ? <h3>Error: {error.error}</h3> : null }
            {isLoading ? <h3>Cargando...</h3> : renderItems()}
        </div>
    )
}
