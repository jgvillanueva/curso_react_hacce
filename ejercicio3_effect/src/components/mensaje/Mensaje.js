import {useEffect, useState} from "react";
import styles from './Mensaje.module.css';

export function Mensaje(props) {
    const [mensaje, setMensaje] = useState();
    useEffect(() => {
        const getData = async () => {
            const url =
                `http://dev.contanimacion.com/api_tablon/api/mensajes/get/${props.id}`;
            const data = await fetch(
                url,
                {
                    method: 'GET',
                }
            )
            const json = await data.json();
            return json;
        };
        const fetchData = async () => {
            if(!props.id)
                return;
            const data = await getData();
            setMensaje(data);
        }
        fetchData()
            .catch((error) => {console.log(error)});
    }, [props.id]);
    return (
        <div className={styles.mensaje}>
            {
                mensaje &&
                <div>
                    <h2>{mensaje.asunto}</h2>
                    <p>{mensaje.mensaje}</p>
                    <p>{mensaje.created_at}</p>
                </div>
            }
        </div>
    )
}
