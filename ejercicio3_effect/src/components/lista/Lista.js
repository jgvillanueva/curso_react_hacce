import styles from './Lista.module.css';

export function Lista (props) {

    const renderMensajes = () => {
        return props.mensajes.map((mensaje) =>
            <li
                key={mensaje.id}
                onClick={() => { props.clickCallback(mensaje.id); }}>
                {mensaje.asunto}
            </li>
        )
    }

    return (
        <ul className={styles.ulLista}>
            {renderMensajes()}
        </ul>
    )
}
