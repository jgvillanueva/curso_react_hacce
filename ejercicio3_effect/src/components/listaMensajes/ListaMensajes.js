import {useEffect, useState} from "react";
import {Lista} from "../lista/Lista";
import {Mensaje} from "../mensaje/Mensaje";
import './ListaMEnsajes.css';

export function ListaMensajes() {
    const estados = {
        PENDING: 'pending',
        SUCCESS: 'success',
    };
    const [mensajes, setMensajes] = useState([]);
    const [status, setStatus] = useState(estados.PENDING);
    const [error, setError] = useState('');
    const [idMensaje, setIdMensaje] = useState();

    useEffect(() => {
        const getData = async () => {
            const url = 'http://dev.contanimacion.com/api_tablon/api/mensajes';
            const data = await fetch(
                url,
                {
                    method: 'GET',
                }
            )
            const json = await data.json();
            return json;
        }

        const fetchData = async () => {
            try {
                const data = await getData();
                setMensajes(data);
                setStatus(estados.SUCCESS);
            } catch (e) {
                setError(e.message);
            }
        }

        fetchData()
            .catch((error) => {console.log(error)});
    }, [estados.SUCCESS]);

    if (status === estados.PENDING) {
        return (
            <h1>Cargando datos...</h1>
        )
    }

    if (error !== '') {
        return (
            <h1>Error cargando datos: {error}</h1>
        )
    }

    const handleSelecionaMensaje = (id) => {
        setIdMensaje(id);
    }

    return (
        <div className="lista-mensajes">
            <h2>Mensajes</h2>
            <Mensaje id={idMensaje} />
            <Lista mensajes={mensajes} clickCallback={handleSelecionaMensaje} />

        </div>
    )
}
