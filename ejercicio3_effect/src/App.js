
import './App.css';
import {ListaMensajes} from "./components/listaMensajes/ListaMensajes";

function App() {
  return (
    <div className="App">
        <div>
            <ul>
                <li>opción 1</li>
                <li>opción 2</li>
                <li>opción 3</li>
            </ul>
        </div>
      <ListaMensajes />
    </div>
  );
}

export default App;
