import wrapPromise from "./wrapPromise";

export const loadData = () => {
    const url = 'http://dev.contanimacion.com/api_tablon/api/mensajes';
    const promise = fetch(url, {method: 'GET'})
        .then(res => res.json())
        .then((result) => result)
    return wrapPromise(promise);
}
