
import './App.css';
import {DebounceInput} from "./components/DebounceInput/DebounceInput";
import {ContanaedorMensajes} from "./components/Mensajes/ContanaedorMensajes";
import {Suspense} from 'react';

function App() {
    const handleInputChange = (valor) => {
        console.log('Ha cambiado el valor del input', valor);
    }
  return (
    <div className="App">
      <DebounceInput
          label="Mi campo debounce"
          handleChange={handleInputChange}
      />

        <Suspense>
            <ContanaedorMensajes />
        </Suspense>

    </div>
  );
}

export default App;
