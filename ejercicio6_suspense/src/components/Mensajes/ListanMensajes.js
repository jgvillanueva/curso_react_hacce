import {loadData} from '../../services/dataLoader';
const loader = loadData();

export function ListanMensajes() {
    const data = loader.read();

    const renderMensajes = () => {
        return data.map((mensaje) => {
            return <li key={mensaje.id}>{mensaje.asunto}</li>
        });
    }

    return (
        <ul>
            {renderMensajes()}
        </ul>
    )
}
