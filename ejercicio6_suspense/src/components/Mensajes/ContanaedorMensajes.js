import {ListanMensajes} from "./ListanMensajes";
import {Suspense} from 'react';

export function ContanaedorMensajes() {
    return (
        <Suspense fallback={<h1>... cargando datos</h1>}>
            <ListanMensajes />
        </Suspense>
    )
}
