import {useEffect, useId, useState} from "react";
import styles from './DebounceInput.module.css';

export function DebounceInput(props) {
    const [inputValue, setInputValue] = useState('');
    const [deboncedInputValue, setDebouncedInputValue] = useState('');

    const {debounceTime, label, handleChange, inputType} = props;

    const id = useId();

    const handleChangeInterno = (event) => {
        setInputValue(event.target.value);
    }

    useEffect(() => {
        const timeoutId = setTimeout(() => {
            setDebouncedInputValue(inputValue);
        }, debounceTime);
        return () => clearTimeout(timeoutId);
    }, [inputValue, debounceTime]);

    useEffect(() => {
        handleChange(deboncedInputValue);
    }, [deboncedInputValue, handleChange]);

    return (
        <div className={styles.inputContainer}>
            <label
                htmlFor={id}
                className={styles.label}
            >
                {label}
            </label>
            <input
                id={id}
                className={styles.input}
                type={inputType}
                onChange={handleChangeInterno}
            />
        </div>
    )
}

DebounceInput.defaultProps = {
    debounceTime: 500,
    inputType: 'text,'
}
