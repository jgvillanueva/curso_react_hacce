const contenedor = document.getElementById('app');
const root = ReactDOM.createRoot(contenedor);

function Saludo({nombre}) {
    return React.createElement(
        'h1',
        {
            className: 'title',
        },
        `Hola ${nombre}`);
}

const saludo = React.createElement(Saludo, {nombre: 'Jorge'});

root.render(saludo);
